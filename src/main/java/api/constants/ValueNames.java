package api.constants;

public interface ValueNames {

    String PROJECT_ID = "ONQM0111";

    String ISSUE_TYPE_TASK = "Task";
    String DESCRIPTION_CREATE_ISSUE = "Creating of an issue using project keys and issue type names using the REST API";
    String USER_NAME = "nata6339";

    String COMMENT = "This is my comment";
}
