package api.pojo;

public class AssignRequestField {

    private Assignee assignee;

    public Assignee getAssignee() {
        return assignee;
    }

    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    public AssignRequestField(Assignee assignee) {
        this.assignee = assignee;
    }
}
