package api.pojo;

public class Assignee {
    private String userName;
    public Assignee (String userName){
        this.userName = userName;
    }
    public String getName() {
        return userName;
    }

    public void setName(String name) {
        this.userName = name;
    }
}
