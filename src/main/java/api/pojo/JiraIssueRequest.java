package api.pojo;

public class JiraIssueRequest {
    private Fields fields;

    public JiraIssueRequest(Project project, String summary, String description, IssueType issuetype){
       this.fields = new Fields(project, summary, description, issuetype);
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }
}
