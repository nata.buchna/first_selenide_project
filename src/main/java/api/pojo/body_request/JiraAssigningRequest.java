package api.pojo.body_request;

import api.pojo.*;

public class JiraAssigningRequest {

    private AssignRequestField fields;

    public JiraAssigningRequest(AssignRequestField assignee) {
        this.fields = assignee;
    }

    public AssignRequestField getFields() {
        return fields;
    }

    public void setFields(AssignRequestField fields) {
        this.fields = fields;
    }
}
