package api.pojo.body_request;

import api.pojo.Fields;

public class JiraCommentRequest {
    private String body;

    public JiraCommentRequest(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
