package api.request;

import api.pojo.body_request.JiraAssigningRequest;
import api.pojo.body_request.JiraCommentRequest;
import api.pojo.body_request.JiraIssueRequest;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public class SendRequest {
    public static Response sendPostRequest(RequestSpecification recSpec, String endPoint,
                                           JiraIssueRequest jiraIssueRequest, int expectedStatusCode) {
        return given()
                .spec(recSpec)
                .when()
                .body(jiraIssueRequest)
                .log().all()
                .post(endPoint)
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .extract().response();
    }

    public static Response sendAssigneeRequest(RequestSpecification recSpec, String endpoint,
                                               JiraAssigningRequest jiraAssigningRequest, int expectedStatusCode){
        return given()
                .spec(recSpec)
                .when()
                .body(jiraAssigningRequest)
                .log().all()
                .put(endpoint)
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .extract().response();
    }

    public static Response sendCommentRequest(RequestSpecification recSpec, String endpoint, JiraCommentRequest commentRequest, int expectedStatusCode){
        return given()
                .spec(recSpec)
                .when()
                .body(commentRequest)
                .log().all()
                .post(endpoint)
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .extract().response();
    }


}
