package api.specification;


import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class JiraRequestSpecification {
    public static RequestSpecification requestSpecification =
            new RequestSpecBuilder()
                    .setBaseUri("https://jira.ithillel.com/")
                    .setContentType(ContentType.JSON)
                    .setAccept("application/json")
                    .setAuth(RestAssured.preemptive().basic("nata6339", "n.buchna"))
                    .addHeader("User-Agent", "hillel_QA")
                    .log(LogDetail.ALL)
                    .build()
                    .filter(new AllureRestAssured());

}
