package ui.hillel_site;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class ConsultationPage {

    private SelenideElement formConsultation() {
        return $x("//form[@id='form-consultation']");
    }

    private SelenideElement inputName() {
        return $x("(//input[@name='name'])[1]");
    }

    private SelenideElement inputMail() {
        return $x("(//input[@type='email'])[1]");
    }

    private SelenideElement telCodSelector() {
        return $x("(//div[@class='iti__flag-container'])[1]");
    }

    private SelenideElement dataCountryCod(String countryCod) {
        return $x("(//div[@class='iti__flag-container'])[1]//ul//li[@data-country-code='" + countryCod + "']");
    }

    private SelenideElement inputTelNumber() {
        return $x("//input[@id='input-tel-consultation']");
    }

    private SelenideElement listThisCourseSelector() {
        return $x("(//span[@class='listbox-btn_content'])[1]");
    }

    private SelenideElement cursName(String name) {
        return $x("//span[text()='" + name + "']");
    }

    private SelenideElement inputComment() {
        return $x("//textarea[@id='input-comment-consultation']");
    }

    private SelenideElement privacyCheckBox() {
        return $x("(//span[@class='checkbox_checkmark'])[1]");
    }

    private SelenideElement submitBtn() {
        return $x("(//button[@type='submit'])[2]");
    }

    private SelenideElement resultText() {
        return $x("//div[@class='form-result_main']/p");
    }




    public ConsultationPage formConsultationIsVisible() {
        formConsultation().shouldBe(visible);
        return this;
    }

    @Step("Put the user name")
    public ConsultationPage filingInputName(String expectedName) {
        inputName().setValue(expectedName);
        return this;
    }
    @Step("Put the user email")
    public ConsultationPage filingInputMail(String expectedMail) {
        inputMail().setValue(expectedMail);
        return this;
    }
    @Step("Choose the country for phone code")
    public ConsultationPage choosesTelCode(String countryCod) {
        telCodSelector().click();
        dataCountryCod(countryCod).scrollIntoView("{block: \"center\"}").click();
        return this;
    }

    @Step("Enter the phone number")
    public ConsultationPage filingInputTelNumber(String telNumber) {
        inputTelNumber().setValue(telNumber);
        return this;
    }
    @Step("Select the name of the course")    public ConsultationPage choosesCourse(String courseName) {
        listThisCourseSelector().click();
        cursName(courseName).click();
        return this;
    }

    @Step("Leave the comment")
    public ConsultationPage enterComment(String comment) {
        inputComment().setValue(comment);
        return this;
    }

    @Step("Click the privacy checkbox")
    public ConsultationPage clickPrivacyCheckBox() {
        privacyCheckBox().click();
        return this;
    }

    @Step("Click submit button")
    public ConsultationPage clickBySubmitBtn() {
        submitBtn().click();
        return this;
    }

    public ConsultationPage checksResultText() {
        resultText().shouldHave(exactText("Відправлено"));
        return this;
    }
}
