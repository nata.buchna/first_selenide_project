package ui.hillel_site;

import com.codeborne.selenide.SelenideElement;
import ui.lms.AuthPage;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class MainPage {

    /**
     * Locators for //nav[@class='site-nav'] block
     * */

    private SelenideElement authPageBtn() {
        return $x("//a[@class='site-nav-btn -lms']");
    }


    /**
     * Locator for consultation button
     * */
    /*private SelenideElement consultationBnt() {
        return $(byId("#btn-consultation-hero"));
    }*/
    private SelenideElement consultationBnt() {
        return $x("//button[@id='btn-consultation-hero']");
    }

    public ConsultationPage clickByConsultationBnt() {
        consultationBnt().click();
        return page(ConsultationPage.class);
    }

    public AuthPage clickByAuthPageBtn() {
        authPageBtn().click();
        return page(AuthPage.class);
    }
}
