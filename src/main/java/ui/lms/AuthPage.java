package ui.lms;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import java.time.Duration;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class AuthPage {
    /**
     * Locators for form login and registrations
     * */
    private SelenideElement form() {
        return $x("//app-login[contains(@class,'page-login')]");
    }

    private SelenideElement registrationBtn() {
        return $x("//a[contains(@href,'registration')]");
    }



@Step ("Check login and registration form")
    public AuthPage checkFormLoginAndRegistrations() {
        form().shouldBe(visible, Duration.ofSeconds(60));
        return this;
    }

    @Step ("Click registration button")
    public RegistrationPage clickByRegistrationBtn() {
        registrationBtn().click();
        return page(RegistrationPage.class);
    }
}
