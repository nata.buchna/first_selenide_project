package ui.lms;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.*;

public class ConfirmEmailPage {

    private SelenideElement emailFields(){
        return $x("//input[@id='confirm-email-code']");
    }

    private SelenideElement submitBtn(){
        return $x("//button[@type='submit']");
    }

    @Step("Enter the email")
    public ConfirmEmailPage fillingEmail(String code){
        emailFields().setValue(code);
        return this;
    }

    @Step ("Click submit button")
    public RecommendationsPage clickBySubmit(){
        submitBtn().click();
        return page(RecommendationsPage.class);
    }



}
