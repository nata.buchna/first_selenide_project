package ui.lms;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import ui.lms.PasswordPage;

import java.util.Arrays;
import java.util.List;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class RegistrationPage {
    /**
     * Locators for registration form
     */
    private SelenideElement nameInput() {
        return $x("//input[@id='contacts_first_name']");
    }

    private SelenideElement surnameInput() {
        return $x("//input[@id='contacts_last_name']");
    }

    private SelenideElement mailInput() {
        return $x("//input[@id='contacts_email']");
    }

    private SelenideElement telephoneInput() {
        return $x("//input[@id='contacts_phone']");
    }

    private SelenideElement dataProcessingCheckBox() {
        return $x("//input[@formcontrolname='data_processing']");
    }

    private SelenideElement termsCheckBox() {
        return $x("//input[@formcontrolname='terms']");
    }

    private SelenideElement nextBtn() {
        return $x("//button[@type='submit']");
    }

    /*public PasswordPage fillingRegistrationForm(String name, String surname, String mail, String telNumber) {
        nameInput().shouldBe(visible).setValue(name);
        surnameInput().setValue(surname);
        mailInput().setValue(mail);
        telephoneInput().setValue(telNumber);
        dataProcessingCheckBox().click();
        termsCheckBox().click();
        nextBtn().click();
        return page(PasswordPage.class);
    }*/

    @Step("Fill up a registration form (name, surname, email and phone number)")
    public PasswordPage fillingRegistrationForm(String... value) {
        List<String> listValue = Arrays.asList(value);
        nameInput().shouldBe(visible).setValue(listValue.get(0));
        surnameInput().setValue(listValue.get(1));
        mailInput().setValue(listValue.get(2));
        telephoneInput().setValue(listValue.get(3));

        dataProcessingCheckBox().click();
        termsCheckBox().click();
        nextBtn().click();
        return page(PasswordPage.class);
    }
}
