package ui.mailinator_service;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class MailinatorMainPage {

    public static String mailinator_main_page = "https://www.mailinator.com";

    private SelenideElement search() {
        return $x("//input[@id='search']");
    }

    @Step("Check confirmation email")
    public PublicMessagePage searchMail(String mail) {
        search().shouldBe(Condition.visible).setValue(mail).pressEnter();
        return Selenide.page(PublicMessagePage.class);
    }
}

