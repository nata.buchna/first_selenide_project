package ui.mailinator_service;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.switchTo;
import static java.time.Duration.ofSeconds;

public class PublicMessagePage {
    String IFRAME_NAME = "html_msg_body";

    private SelenideElement letter(int numberLetter) {
        return $x("(//tr[@ng-repeat='email in emails'])[" + numberLetter + "]");
    }

    private SelenideElement confirmCode() {
        return $x("//p[text()='Confirmation code']/..//h1");
    }

    @Step("Open letter and get confirmation code")
    public String openLetterAndGetConfirmCode() {
        letter(1).shouldBe(visible, ofSeconds(10));
        letter(1).click();
        switchTo().frame(IFRAME_NAME);
        return confirmCode().scrollIntoView("{block: \"center\"}").getText();
    }
}
