package api.jira;

import api.constants.ValueNames;
import api.pojo.body_request.JiraCommentRequest;
import org.apache.hc.core5.http.HttpStatus;
import org.testng.annotations.Test;

import static api.request.SendRequest.sendCommentRequest;
import static api.specification.JiraRequestSpecification.requestSpecification;

public class TestAddComment {

    @Test
    public void testAddComment() {
        JiraCommentRequest commentRequest = new JiraCommentRequest(ValueNames.COMMENT);
        sendCommentRequest(
                requestSpecification,
                "/rest/api/2/issue/ONQM0111-54/comment",
                commentRequest,
                HttpStatus.SC_CREATED
        );
    }

}
