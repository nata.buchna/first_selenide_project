package api.jira;

import api.pojo.AssignRequestField;
import api.pojo.Assignee;
import api.pojo.body_request.JiraAssigningRequest;
import org.apache.hc.core5.http.HttpStatus;
import org.testng.annotations.Test;

import static api.constants.ValueNames.USER_NAME;
import static api.request.SendRequest.sendAssigneeRequest;
import static api.specification.JiraRequestSpecification.requestSpecification;

public class TestAssigningAnIssue {
    @Test
    public void testAssigningAnIssue() {
        JiraAssigningRequest assigningRequest = new JiraAssigningRequest(
                new AssignRequestField(new Assignee(USER_NAME)));
        sendAssigneeRequest(
                requestSpecification,
                "/rest/api/2/issue/ONQM0111-54",
                assigningRequest,
                HttpStatus.SC_NO_CONTENT);
        ;

    }
}