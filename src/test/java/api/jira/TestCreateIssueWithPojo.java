package api.jira;

import api.pojo.IssueType;
import api.pojo.body_request.JiraIssueRequest;
import api.pojo.Project;
import io.restassured.response.Response;
import org.apache.hc.core5.http.HttpStatus;
import org.testng.annotations.Test;

import static api.constants.ValueNames.*;
import static api.request.SendRequest.sendPostRequest;
import static api.specification.JiraRequestSpecification.requestSpecification;
import static org.hamcrest.Matchers.containsString;

public class TestCreateIssueWithPojo {

    @Test
    public void testCreateIssueWithPojo(){

        JiraIssueRequest issueRequest = new JiraIssueRequest(
                new Project(PROJECT_ID),
                "This is new task POJO",
                DESCRIPTION_CREATE_ISSUE,
                new IssueType(ISSUE_TYPE_TASK));
//

        Response response = sendPostRequest(
                requestSpecification,
                "/rest/api/2/issue/",
                issueRequest,
                HttpStatus.SC_CREATED);
        response.then()
                .body("key", containsString(PROJECT_ID));
    }
}
