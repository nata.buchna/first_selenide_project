package com.codeborne.selenide;

import com.codeborne.selenide.WebDriverRunner;
import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.annotations.Listeners;

public class BrowserListener implements ITestListener {
    @Override
    public void onTestFailure(ITestResult iTestResult) {
        System.out.println("Saving lfmskgmrgmb,");
        WebDriver driver = WebDriverRunner.getSelenideDriver().getWebDriver();
        saveScreenshotPNG(driver);
    }

    @Attachment(value = "Page screenshot", type = "image/png", fileExtension = ".png")
    public byte[] saveScreenshotPNG(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
}
