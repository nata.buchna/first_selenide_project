package tests;

import base_package.BaseTest;
import ui.hillel_site.MainPage;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static ui.utils.CommonMethods.randomMail;

public class TestForRequestForConsultation extends BaseTest {
    MainPage mainPage = new MainPage();

    @Test
    public void requestForConsultation() {
        open("https://ithillel.ua/");
        mainPage.clickByConsultationBnt()
                .filingInputName("Autotest")
                .filingInputMail(randomMail())
                .choosesTelCode("ad")
                .filingInputTelNumber("123456")
                .choosesCourse("Java Pro")
//                .enterComment("This is autotest")
                .clickPrivacyCheckBox()
                .clickBySubmitBtn()
                .checksResultText();
    }
}
