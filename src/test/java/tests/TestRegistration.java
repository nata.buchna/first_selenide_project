package tests;

import base_package.BaseTest;
import ui.hillel_site.MainPage;
import io.qameta.allure.Description;
import io.qameta.allure.Owner;
import ui.lms.ConfirmEmailPage;
import ui.mailinator_service.MailinatorMainPage;
import org.openqa.selenium.WindowType;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.*;
import static ui.lms.PasswordPage.PASSWORD;
import static ui.lms.RecommendationsPage.recommendationsPageUrl;
import static ui.utils.CommonMethods.checkingContainsUrl;
import static ui.utils.CommonMethods.randomMail;

public class TestRegistration extends BaseTest {
    MainPage mainPage = new MainPage();
    MailinatorMainPage mailinatorMainPage = new MailinatorMainPage();

    ConfirmEmailPage confirmEmailPage = new ConfirmEmailPage();

    private String mail = randomMail();

    private String confirmCod;

@Owner(value = "Nataliia Buchna")
@Description (value = "Sign up into Hillel website")
    @Test
    public void testRegistration() {
        open("https://ithillel.ua/");
        mainPage.clickByAuthPageBtn()
                .checkFormLoginAndRegistrations()
                .clickByRegistrationBtn()
                .fillingRegistrationForm("Autotest", "Autotest", mail, "302346894")
                .fillingPasswordInput(PASSWORD, PASSWORD);
        switchTo().newWindow(WindowType.TAB);
        switchTo().window(1);
        open("https://www.mailinator.com");

        confirmCod = mailinatorMainPage.searchMail(mail)
                .openLetterAndGetConfirmCode();
        switchTo().window(0);
        confirmEmailPage.fillingEmail(confirmCod)
                .clickBySubmit();
        checkingContainsUrl(recommendationsPageUrl);


    }

}
